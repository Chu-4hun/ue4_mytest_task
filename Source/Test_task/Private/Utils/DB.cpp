// Fill out your copyright notice in the Description page of Project Settings.


#include "Utils/DB.h"
#include "SQLiteDatabase.h"

DEFINE_LOG_CATEGORY_STATIC(DBLog, All, All);

UDB::UDB()
{
	ID = TEXT("@id");
	Username = TEXT("@user_name");
	Gender = TEXT("@gender");
	ImagePath = TEXT("@image");
}


void UDB::InsertUser(FString username, FString gender, FString image)
{
	FSQLiteDatabase* Database = new FSQLiteDatabase();
	if (!(Database->Open(*(FPaths::ProjectDir() + "Content/Database/user.sqlite"),
	                     ESQLiteDatabaseOpenMode::ReadWriteCreate)))
		return;

	if (FSQLitePreparedStatement* PreparedStatement = new FSQLitePreparedStatement)
		if (PreparedStatement->Create(
			*Database, TEXT("INSERT INTO user ('user_name', 'gender', 'image') VALUES (@user_name, @gender,@image);"),
			ESQLitePreparedStatementFlags::Persistent))
		{
			PreparedStatement->SetBindingValueByName(TEXT("@user_name"), "'" + username + "'");
			PreparedStatement->SetBindingValueByName(TEXT("@gender"), "'" + gender + "'");
			PreparedStatement->SetBindingValueByName(TEXT("@image"), "'" + image + "'");

			if (PreparedStatement->Execute())
			{
				PreparedStatement->Destroy();
				delete PreparedStatement;
			}
			Database->Close();
			delete Database;
		}
}

void UDB::UpdateUser(int id, FString username, FString gender, FString image)
{
	FSQLiteDatabase* Database = new FSQLiteDatabase();
	if (!(Database->Open(*(FPaths::ProjectDir() + "Content/Database/user.sqlite"),
	                     ESQLiteDatabaseOpenMode::ReadWriteCreate)))
		return;

	if (FSQLitePreparedStatement* PreparedStatement = new FSQLitePreparedStatement)
		if (PreparedStatement->Create(
			*Database, TEXT("UPDATE user SET user_name=@user_name, gender = @gender, image = @image WHERE id = @id;"),
			ESQLitePreparedStatementFlags::Persistent))
		{
			PreparedStatement->SetBindingValueByName(TEXT("@user_name"), username);
			PreparedStatement->SetBindingValueByName(TEXT("@gender"), gender);
			PreparedStatement->SetBindingValueByName(TEXT("@image"), image);
			PreparedStatement->SetBindingValueByName(TEXT("@id"), id);

			if (PreparedStatement->Execute())
			{
				PreparedStatement->Destroy();
				delete PreparedStatement;
			}
			Database->Close();
			delete Database;
		}
}

void UDB::DeleteUser(int id)
{
	FSQLiteDatabase* Database = new FSQLiteDatabase();
	if (!(Database->Open(*(FPaths::ProjectDir() + "Content/Database/user.sqlite"),
	                     ESQLiteDatabaseOpenMode::ReadWriteCreate)))
		return;

	if (FSQLitePreparedStatement* PreparedStatement = new FSQLitePreparedStatement)
		if (PreparedStatement->Create(*Database, TEXT("DELETE FROM user WHERE id = @id;"),
		                              ESQLitePreparedStatementFlags::Persistent))
		{
			PreparedStatement->SetBindingValueByName(TEXT("@gender"), id);

			if (PreparedStatement->Execute())
			{
				PreparedStatement->Destroy();
				delete PreparedStatement;
			}
			Database->Close();
			delete Database;
		}
}

TArray<FUser_struct> UDB::GetAllUsers() const
{
	TArray<FUser_struct> ReturnValue = TArray<FUser_struct>();
	FSQLiteDatabase* Database = new FSQLiteDatabase();
	if (Database->Open(*(FPaths::ProjectDir() + "Content/Database/user.sqlite"),
	                   ESQLiteDatabaseOpenMode::ReadWriteCreate))
	{
		FSQLitePreparedStatement* PreparedStatement = new FSQLitePreparedStatement;
		PreparedStatement->Create(*Database, TEXT("SELECT * FROM user;"),
		                          ESQLitePreparedStatementFlags::Persistent);
		while (PreparedStatement->Step() != ESQLitePreparedStatementStepResult::Done)
		{
			FUser_struct User_buffer;
			PreparedStatement->GetColumnValueByName(TEXT("id"), User_buffer.id);
			PreparedStatement->GetColumnValueByName(TEXT("user_name"), User_buffer.name);
			PreparedStatement->GetColumnValueByName(TEXT("gender"), User_buffer.gender);
			PreparedStatement->GetColumnValueByName(TEXT("image"), User_buffer.image);
			ReturnValue.Add(User_buffer);
		}
		PreparedStatement->Destroy();
		delete PreparedStatement;
	}

	Database->Close();
	delete Database;


	return ReturnValue;
}

FUser_struct UDB::GetUserByID(int id)
{
	FUser_struct ReturnValue = FUser_struct();
	FSQLiteDatabase* Database = new FSQLiteDatabase();
	if (!(Database->Open(*(FPaths::ProjectDir() + "Content/Database/user.sqlite"),
	                     ESQLiteDatabaseOpenMode::ReadWriteCreate)))
		return ReturnValue;

	if (FSQLitePreparedStatement* PreparedStatement = new FSQLitePreparedStatement)

	{
		PreparedStatement->Create(*Database, TEXT("SELECT * FROM user WHERE id = @id;"),
		                          ESQLitePreparedStatementFlags::Persistent);
		PreparedStatement->SetBindingValueByName(TEXT("@id"), id);

		while (PreparedStatement->Step() != ESQLitePreparedStatementStepResult::Done)
		{
			FUser_struct User_buffer = FUser_struct();
			PreparedStatement->GetColumnValueByName(TEXT("id"), User_buffer.id);
			PreparedStatement->GetColumnValueByName(TEXT("user_name"), User_buffer.name);
			PreparedStatement->GetColumnValueByName(TEXT("gender"), User_buffer.gender);
			PreparedStatement->GetColumnValueByName(TEXT("image"), User_buffer.image);
			ReturnValue = User_buffer;
		}
		PreparedStatement->Destroy();
		delete PreparedStatement;

		Database->Close();
		delete Database;
	}
	return ReturnValue;
}

void UDB::IsValidIndex(int& id)
{
	if (id > GetAllUsers().Num())
		id = GetAllUsers().Num();
}
