// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/UserComponent.h"


DEFINE_LOG_CATEGORY_STATIC(UserLog, Log, All);
UUserComponent::UUserComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UUserComponent::BeginPlay()
{
	Super::BeginPlay();
	DataBase->IsValidIndex(index);
	// ...
	
}



FUser_struct UUserComponent::GetUser()
{
	UE_LOG(UserLog, Log, TEXT("%s"), *DataBase->GetUserByID(index).name);
	return DataBase->GetUserByID(index);
}

TArray<FUser_struct> UUserComponent::GetAllUsers()
{
	UE_LOG(UserLog, Log, TEXT("All users"));
	return DataBase->GetAllUsers();
}

void UUserComponent::DeleteThisUser()
{
	UE_LOG(UserLog, Log, TEXT("User %d deleted"), index);
	return DataBase->DeleteUser(index);
}


void UUserComponent::InsertUser(FUser_struct input)
{
	DataBase->InsertUser(input.name, input.gender, input.image);
}

void UUserComponent::UpdateUser(FUser_struct input)
{
	DataBase->UpdateUser(index, input.name, input.gender, input.image);
}
