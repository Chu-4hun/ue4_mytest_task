// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "DB.generated.h"


/**
 * 
 */
 
USTRUCT(BlueprintType)
struct FUser_struct
{
	GENERATED_BODY()
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Database")
	int id;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Database")
	FString name;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Database")
	FString gender;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Database")
	FString image;
};
 
UCLASS()
class TEST_TASK_API UDB : public UObject
{
	GENERATED_BODY()
	FString ID;
	FString Username;
	FString Gender;
	FString ImagePath;

protected:
	UPROPERTY()
	FUser_struct Users;
public:

	UFUNCTION(BlueprintCallable)
	void InsertUser(FString username, FString gender, FString image);

	UFUNCTION(BlueprintCallable)
	void UpdateUser(int id, FString username, FString gender, FString image);
	
	UFUNCTION(BlueprintCallable)
	void DeleteUser(int id);
	
	UFUNCTION()
	TArray<FUser_struct> GetAllUsers() const;
	
	UFUNCTION()
	FUser_struct GetUserByID(int id);

	UFUNCTION()
	void IsValidIndex(int& id);
	
	UDB();
};
