// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Utils/DB.h"
#include "UserComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TEST_TASK_API UUserComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UUserComponent();
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = "User")
	int index;

	UFUNCTION(BlueprintCallable, Category = "User")
	FUser_struct GetUser();

	UFUNCTION(BlueprintCallable, Category = "User")
	TArray<FUser_struct> GetAllUsers();
	
	UFUNCTION(BlueprintCallable, Category = "User")
	void DeleteThisUser();
	
	UFUNCTION(BlueprintCallable, Category = "User")
	void InsertUser(FUser_struct input);

	UFUNCTION(BlueprintCallable, Category = "User")
	void UpdateUser(FUser_struct input);

protected:
	// Called when the game starts
	UPROPERTY()
	UDB* DataBase;
	

public:	
	
};
